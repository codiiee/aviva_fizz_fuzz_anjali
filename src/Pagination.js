import React from 'react';


export default class Pagination extends React.Component {

	constructor(props) {
	 super(props);
	 this.state = {
		 current: 0
	 }
	}

	handleClick = (item) => {
		if(item === "previous"){
			item = this.state.current > 0 ? this.state.current - 1 : 0
		}else if(item === "next"){
			item = this.state.current < this.props.pageCount-1 ? this.state.current + 1 : this.props.pageCount-1
		}
		this.setState({current:item}, function(){
			this.props.handleClick(item+1)
		}.bind(this));
	}

    render() {
		  		const pages = [...Array(Math.ceil(this.props.pageCount)).keys()];
          return (
            <ul className="pagination mt-2">
						      <li className="page-item" onClick={()=>this.handleClick("previous")}><a href="javascript:void(0)" className="page-link">Previous</a></li>
			                  {pages.map((item, i)=>
												  <li id={"page-"+i} className={this.state.current === i ? "page-item active" : "page-item "} key={i} onClick={()=>this.handleClick(item)}><a href="javascript:void(0)" className="page-link">{item+1}</a></li>
											  )}
							  	<li className="page-item" onClick={()=>this.handleClick("next")}><a href="javascript:void(0)" className="page-link">Next</a></li>
						</ul>
          )



    }
}

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {shallow, mount} from 'enzyme';
import InputBox from './InputBox';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() })

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

xit('calls onClick event on click of a AddInputValue', () => {
  let inputValue = 10;
  const AddInputValue = jest.fn();
  let wrapper = mount(<InputBox SetInputValue={inputValue} onClick={AddInputValue}/>);
  wrapper.find('add-button').simulate('click');
  expect(onClick).toBeCalledWith(0)
})

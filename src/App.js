import React, { Component } from 'react';
import './App.css';
import InputBox from './InputBox'
import List from './List'

class App extends Component {
  constructor() {
    super();
    this.state = {
      dataList: [],
      inputColor: {
        color: ''
      }
    }
  }

  AddInputValue = (enteredValue) => {
    let data = [];
    if (enteredValue > 0 && enteredValue < 1001) {
      for (let i = 1; i <= enteredValue; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
          data = [...data, 'fizzfuzz'];
        }
        else if (i % 5 === 0) {
          (this.TodayDay === 3 ? data = [...data, 'wuzz'] : data = [...data, 'buzz'])
        }
        else if (i % 3 === 0) {
          (this.TodayDay === 3 ? data = [...data, 'wizz'] : data = [...data, 'fizz'])
        }
        else {
          data = [...data, i];
        }
      }
      this.setState({ dataList: data})
    } else {
      alert('Please enter a number between 1 and 1000');
    }
  }

  render() {
    return (
      <div className="container">
        <div className="row justify-content-md-center mt-4">
          <div className="col-6">
              <InputBox AddInputValue={this.AddInputValue} />
          </div>
          <div className="col-6">
              <List listItems={this.state.dataList} color={this.state.inputColor} perPage={10}/>
          </div>
        </div>
      </div>
    );
  }
}

export default App;

import React from 'react';
import ReactDOM from 'react-dom';
import List from './List';
import {shallow, mount} from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Pagination from './Pagination';

Enzyme.configure({ adapter: new Adapter() })

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<List />, div);
  ReactDOM.unmountComponentAtNode(div);
});

xit('starts with value of 1', () => {
   const wrapper = shallow(<Pagination />);
   console.log(wrapper);
   const valueState = wrapper.state().value;
   console.log(valueState);
   expect(valueState).toEqual(1);
});

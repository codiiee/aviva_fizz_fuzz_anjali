import React from 'react';
import ReactDOM from 'react-dom';
import InputBox from './InputBox';
import {shallow, mount} from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Pagination from './Pagination';

Enzyme.configure({ adapter: new Adapter() })

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<InputBox />, div);
  ReactDOM.unmountComponentAtNode(div);
});

describe('when valid input was filled', () => {
    const subscribe = jest.fn();
    const wrapper = mount(<InputBox AddInputValue={subscribe} />);

    wrapper.find('#input-box').simulate('change', {target: {value: 8}});
    wrapper.find('#submit-btn').simulate('click');

    it('should subscribe with correct input', () => {
      expect(subscribe).toHaveBeenCalledWith(8);
    });
});

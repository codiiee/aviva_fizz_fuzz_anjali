import React from 'react';
import Pagination from './Pagination';


class List extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			listItems: props.listItems,
			pageCount: 1,
			displayItems: props.listItems
		}
	}

	componentWillReceiveProps(props){
		if(props.listItems && props.listItems.length !== this.state.listItems.length){
			var pageCount = props.listItems.length > props.perPage ? props.listItems.length/props.perPage : 1;
			var displayItems = props.listItems.length > props.perPage ? props.listItems.filter((item, i)=> i>=0 && i<=props.perPage-1) : props.listItems;
			this.setState({listItems: props.listItems, displayItems: displayItems, pageCount: pageCount});
		}
	}

  handlePageClick = selected => {
		let offset = Math.ceil(selected * this.props.perPage);
		let f_list = this.state.listItems.filter((item, i) => i >=offset-this.props.perPage && i<=offset-1)
		this.setState({ displayItems: f_list}, () => {
		});
	};

	getColor =(val) => {
		let inputColor = {};
		if(val === "fizz"  || val === "wizz" ){
			inputColor= {backgroundColor: '#bee5eb', color: '#0c5460'};
		}else if(val ==="buzz"  || val === "wuzz" ){
			inputColor= {backgroundColor: '#c3e6cb', color: '#155724'};
		}
		return inputColor;
	}

  render() {
        return (
           <div className="col-8">
					 		<ul className="list-group">
	              {this.state.displayItems && this.state.displayItems.map((item, i) =>
	                  <li className="list-group-item" style={this.getColor(item)} key={i} >{item}</li>
	              )}
							</ul>
			  			{this.state.pageCount>1? <Pagination pageCount={this.state.pageCount} handleClick={this.handlePageClick}></Pagination>: ""}
           </div>
        )
  }
}

export default List;

import React, { Component } from 'react';


class InputBox extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      inputValue: '',
    }
  }

  SetInputValue = (e) => {
    let inputValue = e.target.value;
    this.setState({ inputValue: inputValue })
  }

  render() {
    return (
        <form className="col-10 card">
          <div className="form-group p-2">
            <label>Enter Integer value between 1 to 1000:  </label>
            <input id="input-box" type="number" pattern="^([1-9][0-9]{0,2}|1000)$" className="form-control" max='1000' min='1' onChange={this.SetInputValue} />
          </div>
          <div className="form-group p-2">
            <input id="submit-btn" type="button" value="Submit" className="btn btn-primary" onClick={()=>this.props.AddInputValue(this.state.inputValue)} />
          </div>
        </form>
    );
  }
}

export default InputBox;

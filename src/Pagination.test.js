import React from 'react';
import ReactDOM from 'react-dom';
import Pagination from './Pagination';
import {shallow, mount} from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Pagination />, div);
  ReactDOM.unmountComponentAtNode(div);
});

describe('when pagination li clicked', () => {
    const subscribe = jest.fn();
    const wrapper = mount(<Pagination pageCount={10} handleClick={subscribe} />);

    it('page count including previous and next button', () => {
      expect(wrapper.find('li')).toHaveLength(12);
    });

    wrapper.find('#page-6').simulate('click');
    it('should subscribe with correct page on', () => {
      expect(subscribe).toHaveBeenCalledWith(7);
    });
});
